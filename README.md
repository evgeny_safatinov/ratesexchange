# README #

It's rates exchange application for Android OS devices.

Application receives rates data from [CBR](http://www.cbr.ru/scripts/XML_daily.asp "CBR") service and stores data in sqlite database for making exchange when internet on device are not available.

![screen.jpg](https://pp.vk.me/c637931/v637931521/eec5/PK5yMQrx8Wo.jpg)
