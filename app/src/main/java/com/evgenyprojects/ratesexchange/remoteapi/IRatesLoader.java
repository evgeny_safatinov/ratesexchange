package com.evgenyprojects.ratesexchange.remoteapi;

import android.support.annotation.NonNull;

import com.evgenyprojects.ratesexchange.mvp.model.IRatesRequestCallback;

public interface IRatesLoader {
    void loadRates(@NonNull IRatesRequestCallback requestCallback, int attemptNumber);
}
