package com.evgenyprojects.ratesexchange.remoteapi;

import android.support.annotation.NonNull;

import com.evgenyprojects.ratesexchange.ratesdatastructure.fromxml.Rate;
import com.evgenyprojects.ratesexchange.ratesdatastructure.fromxml.Rates;
import com.evgenyprojects.ratesexchange.mvp.model.IRatesRequestCallback;
import com.evgenyprojects.ratesexchange.ratesdatastructure.general.GeneralRate;
import com.evgenyprojects.ratesexchange.ratesdatastructure.general.GeneralRates;
import com.evgenyprojects.ratesexchange.ratesdatastructure.general.IRate;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

public class DefaultRatesLoader implements IRatesLoader {
    private final URL mRatesUrl;
    public DefaultRatesLoader() throws MalformedURLException {
        mRatesUrl = new URL("http://www.cbr.ru/scripts/XML_daily.asp");
    }

    @Override
    public void loadRates(@NonNull IRatesRequestCallback requestCallback, int attemptNumber) {
        try {
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(
                            mRatesUrl.openStream(), "windows-1251"));
            Serializer ser = new Persister();
            Rates rates = ser.read(Rates.class, in);
            in.close();
            if (rates.getRates() != null && rates.getDate() != null) {
                GeneralRates generalRates = new GeneralRates(rates.getDate(),
                        getGeneralRatesFromXmlRates(rates.getRates()));
                requestCallback.onSuccess(generalRates);
            } else {
                requestCallback.onFailed(attemptNumber);
            }

        } catch (Exception e) {
            e.printStackTrace();
            requestCallback.onFailed(attemptNumber);
        }
    }

    @NonNull
    private List<IRate> getGeneralRatesFromXmlRates(@NonNull List<Rate> xmlRates) {
        List<IRate> generalRatesList = new LinkedList<>();
        for(Rate xmlRate : xmlRates) {
            GeneralRate generalRate = new GeneralRate();
            generalRate.setCharCode(xmlRate.getCharCode());
            generalRate.setCost(xmlRate.getCost());
            generalRatesList.add(generalRate);
        }
        return generalRatesList;
    }
}

