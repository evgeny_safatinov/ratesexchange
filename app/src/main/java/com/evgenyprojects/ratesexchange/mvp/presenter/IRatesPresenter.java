package com.evgenyprojects.ratesexchange.mvp.presenter;

import android.support.annotation.NonNull;

import com.evgenyprojects.ratesexchange.mvp.model.RatesModel;
import com.evgenyprojects.ratesexchange.mvp.view.IRatesView;

public interface IRatesPresenter {
    void attachView(@NonNull IRatesView ratesView);
    void attachModel(@NonNull RatesModel ratesModel);
    void onRatesParameterChanged(@NonNull String sourceRate, @NonNull String targetRate, int count);
    void onUpdateAsking();
}
