package com.evgenyprojects.ratesexchange.mvp.model;

import android.support.annotation.NonNull;

public interface IDataReceivingListener {
    void onRequestStatusReceived(@NonNull String dataDate, @NonNull String updateStatus);
    void onNoDataReceived();
}
