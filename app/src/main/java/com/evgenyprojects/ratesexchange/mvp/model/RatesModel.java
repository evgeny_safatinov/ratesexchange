package com.evgenyprojects.ratesexchange.mvp.model;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.evgenyprojects.ratesexchange.database.DbManager;
import com.evgenyprojects.ratesexchange.ratesdatastructure.general.IRate;
import com.evgenyprojects.ratesexchange.ratesdatastructure.general.IRates;
import com.evgenyprojects.ratesexchange.remoteapi.DefaultRatesLoader;
import com.evgenyprojects.ratesexchange.remoteapi.IRatesLoader;

import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class RatesModel implements IRatesModel {
    private static final int TOTAL_ATTEMPTS_COUNT = 3;
    @NonNull
    private final DbManager mDbManager;
    @Nullable
    private List<String> mRatesNames = null;
    @Nullable
    private IRatesLoader mRatesLoader;
    @Nullable
    private IRates mRates;

    public RatesModel(@NonNull DbManager dbManager) {
        mDbManager = dbManager;
        try {
            mRatesLoader = new DefaultRatesLoader();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private void updateDb(@NonNull IRates rates) {
        mDbManager.saveRatesToDb(rates);
    }

    @NonNull
    public String[] getRatesNames() {
        if (mRatesNames != null) {
            return mRatesNames.toArray(new String[mRatesNames.size()]);
        } else {
            return new String[]{};
        }
    }

    @Override
    public void updateData(final @NonNull IDataReceivingListener requestStatusListener) {
        if (mRatesLoader != null) {
            IRatesRequestCallback ratesRequestCallback = new IRatesRequestCallback() {
                @Override
                public void onSuccess(@NonNull IRates rates) {
                    updateDb(rates);
                    mRates = rates;
                    requestStatusListener.onRequestStatusReceived(mRates.getDate(),
                            "Data was updated");
                }

                @Override
                public void onFailed(int attempt) {
                    if (attempt < TOTAL_ATTEMPTS_COUNT) {
                        if (mRatesLoader != null) {
                            try {
                                Thread.sleep(TimeUnit.SECONDS.toMillis(3) * attempt);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            mRatesLoader.loadRates(this, attempt + 1);
                        }
                    } else {
                        mRates = mDbManager.getRates();
                        if (mRates != null) {
                            requestStatusListener.onRequestStatusReceived(mRates.getDate(),
                                    "Data was not updated");
                        } else {
                            requestStatusListener.onNoDataReceived();
                        }
                    }
                }
            };
            mRatesLoader.loadRates(ratesRequestCallback, 1);
        }
        if (mRates != null) {
            mRatesNames = createRateCharCodesList(mRates.getAllRates());
        }
    }

    @NonNull
    private List<String> createRateCharCodesList(@NonNull List<IRate> rates) {
        List<String> ratesCharCodes = new LinkedList<>();
        for (IRate rate : rates) {
            ratesCharCodes.add(rate.getCharCode());
        }
        Collections.sort(ratesCharCodes);
        return ratesCharCodes;
    }

    @Override
    public double getChange(@NonNull String sourceRate, @NonNull String targetRate, int count) {
        try {
            if (mRates != null) {
                return mRates.getChange(sourceRate, targetRate, count);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return Double.NaN;
    }

}
