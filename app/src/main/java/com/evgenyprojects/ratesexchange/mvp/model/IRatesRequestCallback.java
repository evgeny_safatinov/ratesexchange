package com.evgenyprojects.ratesexchange.mvp.model;

import android.support.annotation.NonNull;

import com.evgenyprojects.ratesexchange.ratesdatastructure.general.IRates;

public interface IRatesRequestCallback {
    void onSuccess(@NonNull IRates rates);

    void onFailed(int attemptNumber);
}
