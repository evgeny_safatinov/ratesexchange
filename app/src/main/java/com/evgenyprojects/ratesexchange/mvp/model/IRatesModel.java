package com.evgenyprojects.ratesexchange.mvp.model;

import android.support.annotation.NonNull;

public interface IRatesModel {
    void updateData(@NonNull IDataReceivingListener messageObserver);

    double getChange(@NonNull String sourceRate, @NonNull String targetRate, int count);
}
