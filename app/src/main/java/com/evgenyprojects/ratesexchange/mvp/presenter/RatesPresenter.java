package com.evgenyprojects.ratesexchange.mvp.presenter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.evgenyprojects.ratesexchange.mvp.model.IRatesModel;
import com.evgenyprojects.ratesexchange.mvp.model.IDataReceivingListener;
import com.evgenyprojects.ratesexchange.mvp.model.RatesModel;
import com.evgenyprojects.ratesexchange.mvp.view.IRatesView;

public class RatesPresenter implements IRatesPresenter {
    @Nullable
    private IRatesView mRatesView;
    @Nullable
    private IRatesModel mRatesModel;

    @NonNull
    private final IDataReceivingListener mUpdateMessageObserver = new IDataReceivingListener() {
        @Override
        public void onRequestStatusReceived(@NonNull String dataDate, @NonNull String updateStatus) {
            if (mRatesView != null) {
                mRatesView.onDataReceived(dataDate, updateStatus);
            }
        }

        @Override
        public void onNoDataReceived() {
            if (mRatesView != null) {
                mRatesView.onNoDataReceived();
            }
        }
    };

    @Override
    public void onRatesParameterChanged(@NonNull String sourceRate, @NonNull String targetRate,
                                        int count) {
        if (mRatesView != null && mRatesModel != null) {
            double change = mRatesModel.getChange(sourceRate, targetRate, count);
            mRatesView.onChangeCalculated(change);
        }
    }

    @Override
    public void attachView(@NonNull IRatesView ratesView) {
        mRatesView = ratesView;
    }

    @Override
    public void onUpdateAsking() {
        if (mRatesModel != null) {
            mRatesModel.updateData(mUpdateMessageObserver);
        }
    }

    @Override
    public void attachModel(@NonNull RatesModel ratesModel) {
        mRatesModel = ratesModel;
    }
}
