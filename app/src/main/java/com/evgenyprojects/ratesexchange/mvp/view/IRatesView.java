package com.evgenyprojects.ratesexchange.mvp.view;

import android.support.annotation.NonNull;

public interface IRatesView {
    void onDataReceived(@NonNull String dataDate, @NonNull String updateStatus);

    void onChangeCalculated(double value);

    void onNoDataReceived();
}
