package com.evgenyprojects.ratesexchange.Util;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class RatesArrayAdapter extends ArrayAdapter {
    private final Typeface mTypeface;
    public RatesArrayAdapter(Context context, int resource, Object[] objects) {
        super(context, resource, objects);
        mTypeface = Typeface.createFromAsset(context.getAssets(), "sanfran.otf");
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        TextView v = (TextView) super.getView(position, convertView, parent);
        v.setTypeface(mTypeface);
        return v;
    }

    @NonNull
    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        TextView v = (TextView) super.getView(position, convertView, parent);
        v.setTypeface(mTypeface);
        return v;
    }

}
