package com.evgenyprojects.ratesexchange.Util;

import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;

public class ValueWatcher implements TextWatcher, AdapterView.OnItemSelectedListener {
    @NonNull
    private final IRatesFieldValueChanged mRatesFieldValueChanged;

    public ValueWatcher(@NonNull IRatesFieldValueChanged ratesFieldValueChanged) {
        mRatesFieldValueChanged = ratesFieldValueChanged;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        mRatesFieldValueChanged.onFieldValueChanged();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (editable.length() == 0) {
            return;
        }
        mRatesFieldValueChanged.onFieldValueChanged();
    }
}
