package com.evgenyprojects.ratesexchange.Util;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.evgenyprojects.ratesexchange.R;

public class AboutAppDialog extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final String mFont = "sanfranlight.otf";
        final View v = getActivity().getLayoutInflater().inflate(R.layout.about_dialog_layout,
                null);
        final Typeface mTypeFace = Typeface.createFromAsset(getActivity().getAssets(), mFont);
        final TextView mTitle = (TextView) v.findViewById(R.id.about_dialog_title);
        final TextView mAbout = (TextView) v.findViewById(R.id.about_dialog_message);
        mTitle.setTypeface(mTypeFace);
        mAbout.setTypeface(mTypeFace);
        return new AlertDialog.Builder(getActivity())
                .setView(v)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dismiss();
                    }
                })
                .create();
    }
}
