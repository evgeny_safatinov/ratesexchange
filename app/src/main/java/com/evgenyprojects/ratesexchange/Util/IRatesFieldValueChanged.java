package com.evgenyprojects.ratesexchange.Util;

public interface IRatesFieldValueChanged {
    void onFieldValueChanged();
}
