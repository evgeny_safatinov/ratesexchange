package com.evgenyprojects.ratesexchange.ratesdatastructure.fromxml;

import android.support.annotation.Nullable;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(name = "ValCurs")
public class Rates {
    @Attribute(name = "name")
    private String name;
    @Attribute(name = "Date")
    private String date;
    @ElementList(required = true, inline = true)
    private List<Rate> mRatesList;

    @Nullable
    public List<Rate> getRates() {
        return mRatesList;
    }
    @Nullable
    public String getDate() {
        return date;
    }
}
