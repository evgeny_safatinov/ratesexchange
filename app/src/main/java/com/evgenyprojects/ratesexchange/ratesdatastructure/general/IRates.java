package com.evgenyprojects.ratesexchange.ratesdatastructure.general;

import android.support.annotation.NonNull;

import java.text.ParseException;
import java.util.List;

public interface IRates {
    double getChange(@NonNull String sourceRate, @NonNull String targetRate, int countOfSourceRate)
            throws ParseException;

    @NonNull
    String getDate();

    @NonNull
    List<IRate> getAllRates();
}
