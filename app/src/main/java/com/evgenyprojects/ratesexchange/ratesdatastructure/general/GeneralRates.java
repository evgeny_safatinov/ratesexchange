package com.evgenyprojects.ratesexchange.ratesdatastructure.general;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.text.ParseException;
import java.util.LinkedList;
import java.util.List;

public class GeneralRates implements IRates {
    @NonNull
    private static final String BASE_RATE_CHAR_CODE = "RUB";
    @NonNull
    private String mDate = "no_date";
    @NonNull
    private List<IRate> mRatesList = new LinkedList<>();

    public GeneralRates(@NonNull String date, @NonNull List<IRate> rates) {
        mDate = date;
        mRatesList.addAll(rates);
        GeneralRate baseRate = new GeneralRate();
        baseRate.setCharCode(BASE_RATE_CHAR_CODE);
        baseRate.setCost(1);
        mRatesList.add(baseRate);
    }

    @Override
    public double getChange(@NonNull String sourceRateName, @NonNull String targetRateName,
                            int countOfSourceRate) throws ParseException {
        IRate sourceRate = getRateObjByName(sourceRateName);
        IRate targetRate = getRateObjByName(targetRateName);
        if (sourceRate != null && targetRate != null) {
            return sourceRate.getCost() / targetRate.getCost() * countOfSourceRate;
        }
        return 0;
    }

    @NonNull
    @Override
    public String getDate() {
        return mDate;
    }

    @NonNull
    @Override
    public List<IRate> getAllRates() {
        return mRatesList;
    }

    @Nullable
    private IRate getRateObjByName(@NonNull String rateName) {
        for(IRate r : mRatesList) {
            if (r.getCharCode().equalsIgnoreCase(rateName)) {
                return r;
            }
        }
        return null;
    }
}
