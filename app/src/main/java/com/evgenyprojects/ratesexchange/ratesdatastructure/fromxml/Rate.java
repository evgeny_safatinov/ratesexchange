package com.evgenyprojects.ratesexchange.ratesdatastructure.fromxml;

import android.support.annotation.NonNull;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

@Root(name = "Valute")
public class Rate {
    @Attribute(name = "ID")
    private String mId;
    @Element(name = "NumCode")
    private String numCode;
    @Element(name = "CharCode")
    private String mCharCode;
    @Element(name = "Name")
    private String name;
    @Element(name = "Nominal")
    private int nominal;
    @Element(name = "Value")
    private String value;

    @NonNull
    public String getCharCode() {
        return mCharCode;
    }

    public double getCost() {
        NumberFormat nf = NumberFormat.getInstance(Locale.FRANCE);
        try {
            return nf.parse(value).doubleValue() / nominal;
        } catch (ParseException e) {
            e.printStackTrace();
            return Double.NaN;
        }
    }
}
