package com.evgenyprojects.ratesexchange.ratesdatastructure.general;

import android.support.annotation.NonNull;

public interface IRate {
    double getCost();
    @NonNull
    String getCharCode();
}
