package com.evgenyprojects.ratesexchange.ratesdatastructure.general;

import android.support.annotation.NonNull;

public class GeneralRate implements IRate {
    private String mCharCode;
    private double mCost;

    @Override
    public double getCost() {
        return mCost;
    }

    public void setCharCode(@NonNull String charCode) {
        mCharCode = charCode;
    }

    public void setCost(double cost) {
        mCost = cost;
    }

    @NonNull
    public String getCharCode() {
        return mCharCode;
    }
}
