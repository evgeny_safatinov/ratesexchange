package com.evgenyprojects.ratesexchange;

import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.evgenyprojects.ratesexchange.Util.AboutAppDialog;
import com.evgenyprojects.ratesexchange.Util.IRatesFieldValueChanged;
import com.evgenyprojects.ratesexchange.Util.RatesArrayAdapter;
import com.evgenyprojects.ratesexchange.database.DbManager;
import com.evgenyprojects.ratesexchange.mvp.model.RatesModel;
import com.evgenyprojects.ratesexchange.mvp.presenter.IRatesPresenter;
import com.evgenyprojects.ratesexchange.mvp.presenter.RatesPresenter;
import com.evgenyprojects.ratesexchange.mvp.view.IRatesView;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class MainActivity extends AppCompatActivity {
    @NonNull
    private static final String FROM_POSITION = "last_from_currency";
    @NonNull
    private static final String TO_POSITION = "last_to_currency";
    @NonNull
    private static final String SHARED_PREFERENCE_NAME = "RatesExchange";
    @NonNull
    private final IRatesPresenter mRatesPresenter = new RatesPresenter();

    private TextView mResultText;
    private EditText mCountEditText;
    private TextView mActualDataStateText;
    private Spinner mSpinnerFrom;
    private Spinner mSpinnerTo;
    private ProgressBar mUpdateProgress;
    private LinearLayout mUiInputPanel;
    private FloatingActionButton mDoExchangeButton;
    private FloatingActionButton mUpdateDataButton;

    private final IRatesFieldValueChanged mRatesFieldValueListener = new IRatesFieldValueChanged() {
        @Override
        public void onFieldValueChanged() {
            if(mCountEditText.getText().length() > 0) {
                mRatesPresenter.onRatesParameterChanged(mSpinnerFrom.getSelectedItem().toString(),
                        mSpinnerTo.getSelectedItem().toString(),
                        Integer.parseInt(mCountEditText.getText().toString()));
            }
        }
    };

    private final IRatesView mRatesView = new IRatesView() {

        @Override
        public void onDataReceived(final @NonNull String dataDate,
                                   @NonNull final String updateStatus) {
            Handler mainHandler = new Handler(getMainLooper());

            Runnable updateUiRunnable = new Runnable() {
                @Override
                public void run() {
                    showNormalMode(dataDate, updateStatus);
                }
            };
            mainHandler.post(updateUiRunnable);
        }

        @Override
        public void onChangeCalculated(double value) {
            NumberFormat formatter = new DecimalFormat("#0.00");
            mResultText.setText(formatter.format(value));
        }

        @Override
        public void onNoDataReceived() {
            Handler mainHandler = new Handler(getMainLooper());

            Runnable updateUiRunnable = new Runnable() {
                @Override
                public void run() {
                    showNoDataLoadedMode();
                }
            };
            mainHandler.post(updateUiRunnable);

        }
    };

    private void showDataLoadingMode() {
        hideAllViews();
        mActualDataStateText.setText("Data loading in progress");
        mActualDataStateText.setVisibility(View.VISIBLE);
        mUpdateProgress.setVisibility(View.VISIBLE);
    }

    private void showNoDataLoadedMode() {
        hideAllViews();
        mActualDataStateText.setVisibility(View.VISIBLE);
        mUpdateDataButton.setVisibility(View.VISIBLE);
        mActualDataStateText.setText("No data exists");
    }

    private void showNormalMode(@NonNull String dataDate, @NonNull String updateStatus) {
        hideAllViews();
        mUiInputPanel.setVisibility(View.VISIBLE);
        mDoExchangeButton.setVisibility(View.VISIBLE);
        mActualDataStateText.setVisibility(View.VISIBLE);
        Snackbar.make(mActualDataStateText, updateStatus, Snackbar.LENGTH_SHORT).show();

        mActualDataStateText.setText(getResources()
                .getString(R.string.actual_infromation_date)
                + " " + dataDate);

        RatesArrayAdapter currencyAdapter = new RatesArrayAdapter(MainActivity.this,
                R.layout.currency_spinner_item, mRatesModel.getRatesNames());

        if (isRatesAdapterShouldBeUpdated((ArrayAdapter) mSpinnerFrom.getAdapter(),
                currencyAdapter)) {
            mSpinnerFrom.setAdapter(currencyAdapter);
            mSpinnerTo.setAdapter(currencyAdapter);
            mSpinnerFrom.setSelection(mFromRateInd);
            mSpinnerTo.setSelection(mToRateInd);
        }
    }

    private void hideAllViews() {
        mUiInputPanel.setVisibility(View.INVISIBLE);
        mDoExchangeButton.setVisibility(View.INVISIBLE);
        mActualDataStateText.setVisibility(View.INVISIBLE);
        mUpdateProgress.setVisibility(View.INVISIBLE);
        mUpdateDataButton.setVisibility(View.INVISIBLE);
    }

    private RatesModel mRatesModel;
    private int mFromRateInd = 0;
    private int mToRateInd = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView toolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        toolbarTitle.setTypeface(Typeface.createFromAsset(getAssets(), "sanfranlight.otf"));

        DbManager dbManager = new DbManager(this);
        mRatesPresenter.attachView(mRatesView);

        mRatesModel = new RatesModel(dbManager);
        mRatesPresenter.attachModel(mRatesModel);

        mUiInputPanel = (LinearLayout) findViewById(R.id.uiInputPanel);
        mResultText = (TextView) findViewById(R.id.resultText);
        mCountEditText = (EditText) findViewById(R.id.fromInputNumber);
        mActualDataStateText = (TextView) findViewById(R.id.actualDataStateInfo);
        mSpinnerFrom = (Spinner) findViewById(R.id.currencyFromSpinner);
        mSpinnerTo = (Spinner) findViewById(R.id.currencyToSpinner);

        mUpdateProgress = (ProgressBar) findViewById(R.id.updateProgress);

        ImageButton mChangeDirection = (ImageButton) findViewById(R.id.buttonChangeDirection);
        mChangeDirection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int spinnerFromIndex = mSpinnerFrom.getSelectedItemPosition();
                mSpinnerFrom.setSelection(mSpinnerTo.getSelectedItemPosition());
                mSpinnerTo.setSelection(spinnerFromIndex);
                mResultText.setText("0.0");
                mCountEditText.setText("");
            }
        });

        mSpinnerTo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mResultText.setText("0.0");
                mCountEditText.setText("");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mSpinnerFrom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mResultText.setText("0.0");
                mCountEditText.setText("");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mUpdateDataButton = (FloatingActionButton) findViewById(R.id.updateDataButton);
        mUpdateDataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveRatesPositions(mSpinnerFrom.getSelectedItemPosition(),
                        mSpinnerTo.getSelectedItemPosition());
              loadRatesData();
            }
        });

        mDoExchangeButton = (FloatingActionButton) findViewById(R.id.doExchangeButton);
        mDoExchangeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRatesFieldValueListener.onFieldValueChanged();
            }
        });

        mUiInputPanel.setVisibility(View.INVISIBLE);
        Typeface tf = Typeface.createFromAsset(getAssets(), "sanfran.otf");
        mResultText.setTypeface(tf);
        mCountEditText.setTypeface(tf);
        mActualDataStateText.setTypeface(tf);

        mFromRateInd = getSharedPreferences(SHARED_PREFERENCE_NAME, MODE_PRIVATE)
                .getInt(FROM_POSITION, 0);
        mToRateInd = getSharedPreferences(SHARED_PREFERENCE_NAME, MODE_PRIVATE)
                .getInt(TO_POSITION, 0);
        loadRatesData();
    }

    private void loadRatesData() {
        showDataLoadingMode();
        new Thread() {
            @Override
            public void run() {
                mRatesPresenter.onUpdateAsking();
            }
        }.start();
    }

    @Override
    protected void onDestroy() {
        saveRatesPositions(mSpinnerFrom.getSelectedItemPosition(),
               mSpinnerTo.getSelectedItemPosition());
        super.onDestroy();
    }

    private void saveRatesPositions(int fromPosition, int toPosition) {
        SharedPreferences.Editor mSharedPreferenceEditor =
                getSharedPreferences(SHARED_PREFERENCE_NAME, MODE_APPEND).edit();
        mSharedPreferenceEditor.putInt(FROM_POSITION, fromPosition);
        mSharedPreferenceEditor.putInt(TO_POSITION, toPosition);
        mSharedPreferenceEditor.apply();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_about) {
            new AboutAppDialog().show(getFragmentManager(), "ABOUT_APP_DIALOG_FRAGMENT");
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean isRatesAdapterShouldBeUpdated(@Nullable ArrayAdapter currentAdapter,
                                                  @NonNull ArrayAdapter updateAdapter) {
        return currentAdapter == null || currentAdapter.getCount() != updateAdapter.getCount();
    }
}
