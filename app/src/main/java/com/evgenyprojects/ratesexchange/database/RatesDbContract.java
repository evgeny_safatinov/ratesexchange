package com.evgenyprojects.ratesexchange.database;

import android.provider.BaseColumns;

class RatesDbContract {
    static abstract class RatesEntry implements BaseColumns {
        static final String TABLE_NAME = "ratesentry";
        static final String DATE = "date";
        static final String RATES_DATA = "rates";
        static final String RATES_BASE = "base";
    }

    private static final int JSON_LENGTH = 1000;
    private static final String TEXT_TYPE = " TEXT";
    private static final String RATES_DATA_TYPE = " VARCHAR" + "(" + JSON_LENGTH + ")";
    private static final String DATE_TYPE = " DATE";
    private static final String COMMA_SEP = ",";

    static final String SQL_CREATE_ENTRIES =
                "CREATE TABLE " + RatesEntry.TABLE_NAME + "(" +
                RatesEntry._ID + " INTEGER PRIMARY KEY, " + RatesEntry.DATE +
                DATE_TYPE + COMMA_SEP + RatesEntry.RATES_DATA + RATES_DATA_TYPE + COMMA_SEP +
                RatesEntry.RATES_BASE + TEXT_TYPE + ")";

    static final String SQL_DELETE_ENTRIES = "DROP TABLE " + RatesEntry.TABLE_NAME;

    static final String[] PROJECTION = {
            RatesEntry._ID,
            RatesEntry.DATE,
            RatesEntry.RATES_DATA,
            RatesEntry.RATES_BASE
    };

    static final String DESC_SORT_ORDER = RatesEntry._ID + " DESC";
}
