package com.evgenyprojects.ratesexchange.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.evgenyprojects.ratesexchange.ratesdatastructure.general.GeneralRate;
import com.evgenyprojects.ratesexchange.ratesdatastructure.general.GeneralRates;
import com.evgenyprojects.ratesexchange.ratesdatastructure.general.IRate;
import com.evgenyprojects.ratesexchange.ratesdatastructure.general.IRates;

import java.util.LinkedList;
import java.util.List;

public class DbManager {
    @NonNull
    private static final String RATES_SEPARATOR = ";";
    @NonNull
    private static final String KEY_VALUE_SEPARATOR = ":";
    @NonNull
    private final RatesDbHelper mDbHelper;

    public DbManager(@NonNull Context context) {
        mDbHelper = new RatesDbHelper(context);
    }

    @Nullable
     public IRates getRates() {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        final Cursor c = db.query(RatesDbContract.RatesEntry.TABLE_NAME,
                RatesDbContract.PROJECTION,
                null,
                null,
                null,
                null,
                RatesDbContract.DESC_SORT_ORDER);

        GeneralRates generalRates = null;
        if (c.getCount() > 0) {
            c.moveToFirst();
            String date = c.getString(c.getColumnIndex(RatesDbContract.RatesEntry.DATE));
            String rates = c.getString(c.getColumnIndex(RatesDbContract.RatesEntry.RATES_DATA));
            generalRates = new GeneralRates(date, getIRateListFromString(rates));
        }
        c.close();
        db.close();
        return generalRates;
    }

    public void saveRatesToDb(@NonNull IRates rates) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(RatesDbContract.RatesEntry.DATE, rates.getDate());
        cv.put(RatesDbContract.RatesEntry.RATES_DATA, getDbFormatFromRatesList(rates));
        db.insert(RatesDbContract.RatesEntry.TABLE_NAME, null, cv);
        db.close();
    }

    @NonNull
    private String getDbFormatFromRatesList(@NonNull IRates rates) {
        String answer = "";
        for (IRate r : rates.getAllRates()) {
            answer += r.getCharCode() + KEY_VALUE_SEPARATOR + r.getCost() + RATES_SEPARATOR;
        }
        return answer;
    }

    @NonNull
    private List<IRate> getIRateListFromString(@NonNull String ratesString) {
        String[] ratesAsStrings = ratesString.split(RATES_SEPARATOR);
        List<IRate> rates = new LinkedList<>();
        for (String rateString : ratesAsStrings) {
            String charCode = rateString.split(KEY_VALUE_SEPARATOR)[0];
            double cost = Double.parseDouble(rateString.split(KEY_VALUE_SEPARATOR)[1]);
            GeneralRate generalRate = new GeneralRate();
            generalRate.setCharCode(charCode);
            generalRate.setCost(cost);
            rates.add(generalRate);
        }
        return rates;
    }
}
